const path = require('path');
var BundleTracker = require('webpack-bundle-tracker')
var webpack = require('webpack')

module.exports = {
    mode: 'production',
    entry: ["./app/assets/sass/app.scss", "./app/assets/js/index.js"],
    output: {
        path: path.resolve(__dirname, 'dist'), // output directory
        filename: "[name].js" // name of the generated bundle
    },
    plugins: [
        new BundleTracker({filename: './webpack-stats.json'}),
    ],
    module: {
        rules: [{
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.jsx$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            }

        ],
    },
};
