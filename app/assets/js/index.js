import React from 'react';
import ReactDOM from 'react-dom';
import axios  from 'axios';


const App = () => {
    const[loading, setLoading] = React.useState(false);
    const[items, setItems] = React.useState([]);

    React.useEffect(() => {
        setLoading(true)
        axios.get('/phpapp-react/my-api.php')
        .then(function (response) {
            setLoading(false)
            setItems(response.data)
        })
        .catch(function (error) {
          console.log(error);
        })
    }, []);
  
    return (
        <div className="container">
            {loading && "Loading..."}
            <table className='table'>
                <tr>
                    <td>Src IP</td>
                </tr>
                { items.map((item) => (
                    <tr>
                        <td>{item.srcIp}</td>
                    </tr>
                ))}
                
            </table>
        </div>
    );
}


ReactDOM.render(<App />, document.getElementById('app'));